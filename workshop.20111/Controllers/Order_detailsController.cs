﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using workshop._20111.Data;
using workshop._20111.Models;

namespace workshop._20111.Controllers
{
    public class Order_detailsController : Controller
    {
        private AppDb db = new AppDb();

        // GET: Order_details
        public async Task<ActionResult> Index()
        {
            var order_details = db.Order_details.Include(o => o.Orders).Include(o => o.Products);
            return View(await order_details.ToListAsync());
        }

        // GET: Order_details/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_details order_details = await db.Order_details.FindAsync(id);
            if (order_details == null)
            {
                return HttpNotFound();
            }
            return View(order_details);
        }

        // GET: Order_details/Create
        public ActionResult Create()
        {
            ViewBag.order_id = new SelectList(db.Orders, "order_id", "ship_name");
            ViewBag.product_id = new SelectList(db.Products, "product_id", "product_name");
            return View();
        }

        // POST: Order_details/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "order_id,product_id,unit_price,quantity,discount")] Order_details order_details)
        {
            if (ModelState.IsValid)
            {
                db.Order_details.Add(order_details);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.order_id = new SelectList(db.Orders, "order_id", "ship_name", order_details.order_id);
            ViewBag.product_id = new SelectList(db.Products, "product_id", "product_name", order_details.product_id);
            return View(order_details);
        }

        // GET: Order_details/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_details order_details = await db.Order_details.FindAsync(id);
            if (order_details == null)
            {
                return HttpNotFound();
            }
            ViewBag.order_id = new SelectList(db.Orders, "order_id", "ship_name", order_details.order_id);
            ViewBag.product_id = new SelectList(db.Products, "product_id", "product_name", order_details.product_id);
            return View(order_details);
        }

        // POST: Order_details/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "order_id,product_id,unit_price,quantity,discount")] Order_details order_details)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order_details).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.order_id = new SelectList(db.Orders, "order_id", "ship_name", order_details.order_id);
            ViewBag.product_id = new SelectList(db.Products, "product_id", "product_name", order_details.product_id);
            return View(order_details);
        }

        // GET: Order_details/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order_details order_details = await db.Order_details.FindAsync(id);
            if (order_details == null)
            {
                return HttpNotFound();
            }
            return View(order_details);
        }

        // POST: Order_details/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Order_details order_details = await db.Order_details.FindAsync(id);
            db.Order_details.Remove(order_details);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
