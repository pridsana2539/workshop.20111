﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(workshop._20111.Startup))]
namespace workshop._20111
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
