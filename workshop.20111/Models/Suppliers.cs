namespace workshop._20111.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Suppliers
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Suppliers()
        {
            Products = new HashSet<Products>();
        }

        [Key]
        public int supplier_id { get; set; }

        [Required]
        [StringLength(20)]
        public string company_name { get; set; }

        [Required]
        public string contact_name { get; set; }

        public string contact_title { get; set; }

        [Required]
        [StringLength(100)]
        public string address { get; set; }

        [Required]
        [StringLength(20)]
        public string city { get; set; }

        public string region { get; set; }

        [Required]
        public string postal_code { get; set; }

        [Required]
        [StringLength(20)]
        public string country { get; set; }

        [Required]
        [StringLength(20)]
        public string phone { get; set; }

        public string fax { get; set; }

        public string homepage { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Products> Products { get; set; }
    }
}
