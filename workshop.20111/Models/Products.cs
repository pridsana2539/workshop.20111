namespace workshop._20111.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Products
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Products()
        {
            Order_details = new HashSet<Order_details>();
        }

        [Key]
        public int product_id { get; set; }

        [Required]
        [StringLength(200)]
        public string product_name { get; set; }

        public int quantity_per_unit { get; set; }

        public decimal unit_price { get; set; }

        public int unit_in_stock { get; set; }

        public int unit_on_order { get; set; }

        public int reorder_level { get; set; }

        public int discontinued { get; set; }

        public int supplier_id { get; set; }

        public int category_id { get; set; }

        public virtual Categories Categories { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order_details> Order_details { get; set; }

        public virtual Suppliers Suppliers { get; set; }

        [Column("imgepath")]
        [Display(Name = "ImgePath")]
        public string ImgePath { get; set; }
    }
}
