namespace workshop._20111.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Employees
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employees()
        {
            Orders = new HashSet<Orders>();
        }

        [Key]
        public int employee_id { get; set; }

        [Required]
        [StringLength(100)]
        public string first_name { get; set; }

        [Required]
        [StringLength(100)]
        public string last_name { get; set; }

        [Required]
        public string title { get; set; }

        public string title_of_countesy { get; set; }

        public DateTime birth_date { get; set; }

        public DateTime hire_date { get; set; }

        [Required]
        public string address { get; set; }

        public string city { get; set; }

        public string region { get; set; }

        [Required]
        public string postal_code { get; set; }

        [Required]
        [StringLength(20)]
        public string country { get; set; }

        [Required]
        [StringLength(20)]
        public string home_phone { get; set; }

        public string extension { get; set; }

        public string notes { get; set; }

        public int reports_to { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
