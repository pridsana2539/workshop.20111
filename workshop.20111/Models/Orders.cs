namespace workshop._20111.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Orders
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Orders()
        {
            Order_details = new HashSet<Order_details>();
        }

        [Key]
        public int order_id { get; set; }

        public DateTime order_date { get; set; }

        public DateTime required_date { get; set; }

        public int freight { get; set; }

        [Required]
        public string ship_name { get; set; }

        [Required]
        public string ship_address { get; set; }

        public string ship_city { get; set; }

        public string ship_region { get; set; }

        [Required]
        public string ship_postal_code { get; set; }

        [Required]
        public string Ship_country { get; set; }

        public int customer_id { get; set; }

        public int employee_id { get; set; }

        public int ship_via { get; set; }

        public virtual Customers Customers { get; set; }

        public virtual Employees Employees { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order_details> Order_details { get; set; }

        public virtual Shippers Shippers { get; set; }
    }
}
