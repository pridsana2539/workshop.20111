namespace workshop._20111.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial02 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Categories", "create_by", c => c.String(maxLength: 100));
            AddColumn("dbo.Categories", "create_date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Categories", "create_date");
            DropColumn("dbo.Categories", "create_by");
        }
    }
}
