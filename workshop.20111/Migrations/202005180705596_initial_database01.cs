namespace workshop._20111.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial_database01 : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Categories",
            //    c => new
            //        {
            //            category_id = c.Int(nullable: false, identity: true),
            //            category_name = c.String(nullable: false, maxLength: 10),
            //            description = c.String(nullable: false, maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.category_id);
            
            //CreateTable(
            //    "dbo.Products",
            //    c => new
            //        {
            //            product_id = c.Int(nullable: false, identity: true),
            //            product_name = c.String(nullable: false, maxLength: 200),
            //            quantity_per_unit = c.Int(nullable: false),
            //            unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            unit_in_stock = c.Int(nullable: false),
            //            unit_on_order = c.Int(nullable: false),
            //            reorder_level = c.Int(nullable: false),
            //            discontinued = c.Int(nullable: false),
            //            supplier_id = c.Int(nullable: false),
            //            category_id = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.product_id)
            //    .ForeignKey("dbo.Categories", t => t.category_id, cascadeDelete: true)
            //    .ForeignKey("dbo.Suppliers", t => t.supplier_id, cascadeDelete: true)
            //    .Index(t => t.supplier_id)
            //    .Index(t => t.category_id);
            
            //CreateTable(
            //    "dbo.Order_details",
            //    c => new
            //        {
            //            order_id = c.Int(nullable: false),
            //            product_id = c.Int(nullable: false),
            //            unit_price = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            quantity = c.Int(nullable: false),
            //            discount = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.order_id, t.product_id })
            //    .ForeignKey("dbo.Orders", t => t.order_id, cascadeDelete: true)
            //    .ForeignKey("dbo.Products", t => t.product_id, cascadeDelete: true)
            //    .Index(t => t.order_id)
            //    .Index(t => t.product_id);
            
            //CreateTable(
            //    "dbo.Orders",
            //    c => new
            //        {
            //            order_id = c.Int(nullable: false, identity: true),
            //            order_date = c.DateTime(nullable: false),
            //            required_date = c.DateTime(nullable: false),
            //            freight = c.Int(nullable: false),
            //            ship_name = c.String(nullable: false),
            //            ship_address = c.String(nullable: false),
            //            ship_city = c.String(),
            //            ship_region = c.String(),
            //            ship_postal_code = c.String(nullable: false),
            //            Ship_country = c.String(nullable: false),
            //            customer_id = c.Int(nullable: false),
            //            employee_id = c.Int(nullable: false),
            //            ship_via = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.order_id)
            //    .ForeignKey("dbo.Customers", t => t.customer_id, cascadeDelete: true)
            //    .ForeignKey("dbo.Employees", t => t.employee_id, cascadeDelete: true)
            //    .ForeignKey("dbo.Shippers", t => t.ship_via, cascadeDelete: true)
            //    .Index(t => t.customer_id)
            //    .Index(t => t.employee_id)
            //    .Index(t => t.ship_via);
            
            //CreateTable(
            //    "dbo.Customers",
            //    c => new
            //        {
            //            customer_id = c.Int(nullable: false, identity: true),
            //            company_name = c.String(nullable: false, maxLength: 20),
            //            contact_name = c.String(nullable: false),
            //            contact_title = c.String(),
            //            address = c.String(nullable: false, maxLength: 100),
            //            city = c.String(nullable: false, maxLength: 20),
            //            region = c.String(),
            //            postal_code = c.String(nullable: false),
            //            country = c.String(nullable: false, maxLength: 20),
            //            phone = c.String(nullable: false, maxLength: 20),
            //            fax = c.String(),
            //        })
            //    .PrimaryKey(t => t.customer_id);
            
            //CreateTable(
            //    "dbo.Employees",
            //    c => new
            //        {
            //            employee_id = c.Int(nullable: false, identity: true),
            //            first_name = c.String(nullable: false, maxLength: 100),
            //            last_name = c.String(nullable: false, maxLength: 100),
            //            title = c.String(nullable: false),
            //            title_of_countesy = c.String(),
            //            birth_date = c.DateTime(nullable: false),
            //            hire_date = c.DateTime(nullable: false),
            //            address = c.String(nullable: false),
            //            city = c.String(),
            //            region = c.String(),
            //            postal_code = c.String(nullable: false),
            //            country = c.String(nullable: false, maxLength: 20),
            //            home_phone = c.String(nullable: false, maxLength: 20),
            //            extension = c.String(),
            //            notes = c.String(),
            //            reports_to = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.employee_id);
            
            //CreateTable(
            //    "dbo.Shippers",
            //    c => new
            //        {
            //            shipper_id = c.Int(nullable: false, identity: true),
            //            company_name = c.String(nullable: false),
            //            phone = c.String(nullable: false),
            //        })
            //    .PrimaryKey(t => t.shipper_id);
            
            //CreateTable(
            //    "dbo.Suppliers",
            //    c => new
            //        {
            //            supplier_id = c.Int(nullable: false, identity: true),
            //            company_name = c.String(nullable: false, maxLength: 20),
            //            contact_name = c.String(nullable: false),
            //            contact_title = c.String(),
            //            address = c.String(nullable: false, maxLength: 100),
            //            city = c.String(nullable: false, maxLength: 20),
            //            region = c.String(),
            //            postal_code = c.String(nullable: false),
            //            country = c.String(nullable: false, maxLength: 20),
            //            phone = c.String(nullable: false, maxLength: 20),
            //            fax = c.String(),
            //            homepage = c.String(),
            //        })
            //    .PrimaryKey(t => t.supplier_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "supplier_id", "dbo.Suppliers");
            DropForeignKey("dbo.Order_details", "product_id", "dbo.Products");
            DropForeignKey("dbo.Orders", "ship_via", "dbo.Shippers");
            DropForeignKey("dbo.Order_details", "order_id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "employee_id", "dbo.Employees");
            DropForeignKey("dbo.Orders", "customer_id", "dbo.Customers");
            DropForeignKey("dbo.Products", "category_id", "dbo.Categories");
            DropIndex("dbo.Orders", new[] { "ship_via" });
            DropIndex("dbo.Orders", new[] { "employee_id" });
            DropIndex("dbo.Orders", new[] { "customer_id" });
            DropIndex("dbo.Order_details", new[] { "product_id" });
            DropIndex("dbo.Order_details", new[] { "order_id" });
            DropIndex("dbo.Products", new[] { "category_id" });
            DropIndex("dbo.Products", new[] { "supplier_id" });
            DropTable("dbo.Suppliers");
            DropTable("dbo.Shippers");
            DropTable("dbo.Employees");
            DropTable("dbo.Customers");
            DropTable("dbo.Orders");
            DropTable("dbo.Order_details");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
